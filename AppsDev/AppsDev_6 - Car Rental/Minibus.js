"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Minibus = /** @class */ (function () {
    function Minibus(input) {
        this.minibusCollection = [];
        this.minibusCollection = input;
    }
    Minibus.prototype.getAllofType = function () {
        var minibusList = [];
        for (var _i = 0, _a = this.minibusCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "minibus") {
                minibusList.push(index);
            }
        }
        return minibusList;
    };
    return Minibus;
}());
exports.Minibus = Minibus;
