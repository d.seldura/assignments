<?php
session_start();
if (!isset($_SESSION["firstRun"]))
$_SESSION["firstRun"] = TRUE;
if (!isset($_SESSION["score"]))
$_SESSION["score"] = 0;

function spin() {
    //storage of images
    $images = array("",".\img\bigwin.jpg", ".\img\\tribar.jpg" , ".\img\seven.jpg" , ".\img\cherry.jpg", ".\img\banana.jpg", ".\img\lemon.jpg", ".\img\melon.jpg", ".\img\orange.jpg", ".\img\grape.jpg");
    //generate the value of the spin
    $spin = array(rand(1,9),rand(1,9),rand(1,9));
    //+++++++++++++++++++++++++++++++++++++++++++++++++
    //comment this out!
    print "results of the spin: ".$spin[0].$spin[1].$spin[2];
    //+++++++++++++++++++++++++++++++++++++++++++++++++
    //first run setting
    if ($_SESSION["firstRun"]==TRUE){
        $_SESSION["firstRun"]=FALSE;
        return "<img src=\"".$images[1]."\">"."<img src=\"".$images[1]."\">"."<img src=\"".$images[1]."\">";
    }
    //INSERT YOUR SCORING LOGIC HERE ONE HAS BEEN DONE FOR YOU
    if ($spin[0]!=$spin[1]&&$spin[1]!=$spin[2])
        $_SESSION["score"]+=15;


    return "<img src=\"".$images[$spin[0]]."\">.<img src=\"".$images[$spin[1]]."\">.<img src=\"".$images[$spin[2]]."\">";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Slot Machine</title>
    <link rel="stylesheet" href="myStyle.css">
</head>

<body>
    <div class="output">
        <div class="wintable float-left">
            <div class="win-data">
                <table>
                    <th>Win Chart</th>
                    <tr>
                        <td>
                            <img src=".\img\bigwin.jpg">
                            <img src=".\img\bigwin.jpg">
                            <img src=".\img\bigwin.jpg"></td>
                        <td>
                            <span>200</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\tribar.jpg">
                            <img src=".\img\tribar.jpg">
                            <img src=".\img\tribar.jpg">
                        </td>
                        <td>
                            <span>50</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\seven.jpg">
                            <img src=".\img\seven.jpg">
                            <img src=".\img\seven.jpg">
                        </td>
                        <td>
                            <span>20</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\cherry.jpg">
                            <img src=".\img\cherry.jpg">
                            <img src=".\img\cherry.jpg">
                        </td>
                        <td>
                            <span>15</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\banana.jpg">
                            <img src=".\img\banana.jpg">
                            <img src=".\img\banana.jpg">
                        </td>
                        <td>
                            <span>15</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\lemon.jpg">
                            <img src=".\img\lemon.jpg">
                            <img src=".\img\lemon.jpg">
                        </td>
                        <td>
                            <span>15</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\melon.jpg">
                            <img src=".\img\melon.jpg">
                            <img src=".\img\melon.jpg">
                        </td>
                        <td>
                            <span>15</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\orange.jpg">
                            <img src=".\img\orange.jpg">
                            <img src=".\img\orange.jpg">
                        </td>
                        <td>
                            <span>15</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img src=".\img\grape.jpg">
                            <img src=".\img\grape.jpg">
                            <img src=".\img\grape.jpg">
                        </td>
                        <td>
                            <span>15</span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        Any combination of fruits
                        </td>
                        <td>
                            <span>5</span>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="float-right">
            <div class="reel">
                <?php
                    echo spin();
                ?>
            </div>
            <table>
            <tr>
            <td>
                <form action="slot-machine.php" method="post">
                    <input type="submit" value="Spin!">
                </form>
            </td>
            <td>
                <form action="reset.php" method="post">
                    <input type="submit" value="Reset">
                </form>
            </td>
            </tr>
            </table>
            <div class="info flex-row">
                <div class="border component" id="score">
                    Score<span>
                    <?php
                    print $_SESSION["score"];
                    ?>
                    </span>
                </div>
            </div>
        </div>
    </div>
</body>
</html>