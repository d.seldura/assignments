(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Minibus = /** @class */ (function () {
    function Minibus(input) {
        this.minibusCollection = [];
        this.minibusCollection = input;
    }
    Minibus.prototype.getAllofType = function () {
        var minibusList = [];
        for (var _i = 0, _a = this.minibusCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "minibus") {
                minibusList.push(index);
            }
        }
        return minibusList;
    };
    return Minibus;
}());
exports.Minibus = Minibus;

},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Motorcycle = /** @class */ (function () {
    function Motorcycle(input) {
        this.motorcycleCollection = [];
        this.motorcycleCollection = input;
    }
    Motorcycle.prototype.getAllofType = function () {
        var motorcycleList = [];
        for (var _i = 0, _a = this.motorcycleCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "motorcycle") {
                motorcycleList.push(index);
            }
        }
        return motorcycleList;
    };
    return Motorcycle;
}());
exports.Motorcycle = Motorcycle;

},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ObjectDisplay = /** @class */ (function () {
    function ObjectDisplay(input) {
        this.target = document.getElementById(input);
        this.storage = [];
        this.parsedData = "";
        this.currentID = 0;
    }
    ObjectDisplay.prototype.addToStorage = function (input) {
        this.storage.push(input);
    };
    ObjectDisplay.prototype.addToDisplay = function () {
        var update = "";
        for (var _i = 0, _a = this.storage; _i < _a.length; _i++) {
            var index = _a[_i];
            update += "<div class=\"squareish\" id=\"option_" + this.currentID + "\"><h4>" + index.brand + " " + index.model + "</h4><p>Php." + index.pricePerDay + "/day</p>" + "<button id=\"RentOption_" + this.currentID + "\" value=\"" + this.currentID + "\">RENT</button></div>";
            console.log(update);
            this.currentID += 1;
            console.log("currentID " + this.currentID);
        }
        this.parsedData = update;
    };
    ObjectDisplay.prototype.cleanSlate = function () {
        this.storage = [];
        this.parsedData = "";
        this.currentID = 0;
        this.display();
    };
    ObjectDisplay.prototype.display = function () {
        this.target.innerHTML = this.parsedData;
    };
    return ObjectDisplay;
}());
exports.ObjectDisplay = ObjectDisplay;

},{}],4:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var rentalChoices = document.getElementsByTagName("button");
var receiptVisibility = document.getElementById("receipt");
var Order = /** @class */ (function () {
    function Order() {
        var _this = this;
        var _loop_1 = function (i) {
            rentalChoices[i].addEventListener("click", function (e) { return _this.buttonEvent(Number(rentalChoices[i].value)); });
            console.log("RentalChoices" + rentalChoices[i].value);
        };
        for (var i = 0; i < rentalChoices.length; i++) {
            _loop_1(i);
        }
    }
    Order.prototype.buttonEvent = function (n) {
        //console.log(n);
        receiptVisibility.style.display = "block";
        var selectedOption = document.getElementById("option_" + n);
        var receiptData = selectedOption.innerText.split("\n");
        //console.log(receiptData);
        var userNameInput = document.getElementById("userName");
        var numberInput = document.getElementById("phoneNumber");
        var daysInput = document.getElementById("days");
        document.getElementById("vehicle").innerHTML = receiptData[0];
        document.getElementById("user").innerHTML = userNameInput.value == "" ? "Name Unavailable" : userNameInput.value;
        document.getElementById("number").innerHTML = numberInput.value == "" ? "Number Unavailable" : numberInput.value;
        document.getElementById("cost").innerHTML = receiptData[2];
        document.getElementById("daysToRent").innerHTML = daysInput.value == "" ? "1" : daysInput.value;
        var costStr = receiptData[2].match(/(\d+)/);
        if (numberInput.value == "")
            document.getElementById("balance").innerHTML = 1 * Number(costStr[0]);
        else
            document.getElementById("balance").innerHTML = Number(daysInput.value) * Number(costStr[0]);
    };
    Order.prototype.hideReceipt = function () {
        receiptVisibility.style.display = "none";
    };
    return Order;
}());
exports.Order = Order;

},{}],5:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Pickup = /** @class */ (function () {
    function Pickup(input) {
        this.pickupCollection = [];
        this.pickupCollection = input;
    }
    Pickup.prototype.getAllofType = function () {
        var pickupList = [];
        for (var _i = 0, _a = this.pickupCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "pickup") {
                pickupList.push(index);
            }
        }
        return pickupList;
    };
    return Pickup;
}());
exports.Pickup = Pickup;

},{}],6:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var SUV = /** @class */ (function () {
    function SUV(input) {
        this.suvCollection = [];
        this.suvCollection = input;
    }
    SUV.prototype.getAllofType = function () {
        var suvList = [];
        for (var _i = 0, _a = this.suvCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "suv") {
                suvList.push(index);
            }
        }
        return suvList;
    };
    return SUV;
}());
exports.SUV = SUV;

},{}],7:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Sedan = /** @class */ (function () {
    function Sedan(input) {
        this.sedanCollection = [];
        this.sedanCollection = input;
    }
    Sedan.prototype.getAllofType = function () {
        var sedanList = [];
        for (var _i = 0, _a = this.sedanCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "sedan") {
                sedanList.push(index);
            }
        }
        return sedanList;
    };
    return Sedan;
}());
exports.Sedan = Sedan;

},{}],8:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Van = /** @class */ (function () {
    function Van(input) {
        this.vanCollection = [];
        this.vanCollection = input;
    }
    Van.prototype.getAllofType = function () {
        var vanList = [];
        for (var _i = 0, _a = this.vanCollection; _i < _a.length; _i++) {
            var index = _a[_i];
            if (index.type == "van") {
                vanList.push(index);
            }
        }
        return vanList;
    };
    return Van;
}());
exports.Van = Van;

},{}],9:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VehicleList = /** @class */ (function () {
    function VehicleList() {
        this.vehicleCollection = [];
        this.vehicleCollection = [
            //motorcycle
            { noOfWheels: 2, model: 'Mio', brand: 'Yamaha', type: 'motorcycle', pricePerDay: 448 },
            { noOfWheels: 2, model: 'Scoopy', brand: 'Honda', type: 'motorcycle', pricePerDay: 350 },
            { noOfWheels: 2, model: 'Beat 110cc', brand: 'Honda', type: 'motorcycle', pricePerDay: 448 },
            { noOfWheels: 2, model: 'Nmax 155cc', brand: 'Yamaha', type: 'motorcycle', pricePerDay: 599 },
            { noOfWheels: 2, model: 'Cafe Racer 152cc', brand: 'Keeway', type: 'motorcycle', pricePerDay: 600 },
            //sedan
            { noOfWheels: 4, model: 'Mirage G4', brand: 'Mitsubishi', type: 'sedan', pricePerDay: 1500 },
            { noOfWheels: 4, model: 'Rio', brand: 'Kia', type: 'sedan', pricePerDay: 1500 },
            { noOfWheels: 4, model: 'Accent', brand: 'Hyundai', type: 'sedan', pricePerDay: 1500 },
            { noOfWheels: 4, model: 'Almera', brand: 'Nissan', type: 'sedan', pricePerDay: 1500 },
            { noOfWheels: 4, model: 'Vios', brand: 'Toyota', type: 'sedan', pricePerDay: 1500 },
            //SUV
            { noOfWheels: 4, model: 'RAV4', brand: 'Toyota', type: 'suv', pricePerDay: 2500 },
            { noOfWheels: 4, model: 'Rogue', brand: 'Nissan', type: 'suv', pricePerDay: 2500 },
            { noOfWheels: 4, model: 'Sorento', brand: 'Kia', type: 'suv', pricePerDay: 2500 },
            { noOfWheels: 4, model: 'Explorer', brand: 'Ford', type: 'suv', pricePerDay: 2500 },
            { noOfWheels: 4, model: 'CR-V', brand: 'Honda', type: 'suv', pricePerDay: 2500 },
            //pickup
            { noOfWheels: 4, model: 'Colorado', brand: 'Chevrolet', type: 'pickup', pricePerDay: 3000 },
            { noOfWheels: 4, model: 'Ranger', brand: 'Ford', type: 'pickup', pricePerDay: 3000 },
            { noOfWheels: 4, model: 'D-Max', brand: 'Isuzu', type: 'pickup', pricePerDay: 3000 },
            { noOfWheels: 4, model: 'BT-50', brand: 'Mazda', type: 'pickup', pricePerDay: 3000 },
            { noOfWheels: 4, model: 'L-200', brand: 'Mitsubishi', type: 'pickup', pricePerDay: 3000 },
            //van
            { noOfWheels: 4, model: 'Starex', brand: 'Hyundai', type: 'van', pricePerDay: 3200 },
            { noOfWheels: 4, model: 'HiAce', brand: 'Toyota', type: 'van', pricePerDay: 3200 },
            { noOfWheels: 4, model: 'UrVan', brand: 'Nissan', type: 'van', pricePerDay: 3200 },
            { noOfWheels: 4, model: 'Carnival', brand: 'Kia', type: 'van', pricePerDay: 3200 },
            { noOfWheels: 4, model: 'Innova', brand: 'Toyota', type: 'van', pricePerDay: 3200 },
            //minibus
            { noOfWheels: 4, model: 'H350', brand: 'Hyundai', type: 'minibus', pricePerDay: 4500 },
            { noOfWheels: 4, model: 'Coaster', brand: 'Toyota', type: 'minibus', pricePerDay: 4500 },
            { noOfWheels: 4, model: 'L300', brand: 'Mitsubishi', type: 'minibus', pricePerDay: 4500 },
            { noOfWheels: 4, model: 'Town', brand: 'Chrysler', type: 'minibus', pricePerDay: 4500 },
            { noOfWheels: 4, model: 'Besta', brand: 'Kia', type: 'minibus', pricePerDay: 4500 }
        ];
    }
    VehicleList.prototype.getAllVehicles = function () {
        return this.vehicleCollection;
    };
    return VehicleList;
}());
exports.VehicleList = VehicleList;

},{}],10:[function(require,module,exports){
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Motorcycle_1 = require("./Motorcycle");
var Minibus_1 = require("./Minibus");
var Pickup_1 = require("./Pickup");
var Sedan_1 = require("./Sedan");
var SUV_1 = require("./SUV");
var Van_1 = require("./Van");
var VehicleList_1 = require("./VehicleList");
var ObjectDisplay_1 = require("./ObjectDisplay");
var Order_1 = require("./Order");
var vehiclesRental = new VehicleList_1.VehicleList();
var motorcycleRental = new Motorcycle_1.Motorcycle(vehiclesRental.getAllVehicles());
//7
var pickupRental = new Pickup_1.Pickup(vehiclesRental.getAllVehicles());
//8
var sedanRental = new Sedan_1.Sedan(vehiclesRental.getAllVehicles());
//9
var suvRental = new SUV_1.SUV(vehiclesRental.getAllVehicles());
//10
var vanRental = new Van_1.Van(vehiclesRental.getAllVehicles());
var minibusRental = new Minibus_1.Minibus(vehiclesRental.getAllVehicles());
var showOptions = new ObjectDisplay_1.ObjectDisplay("vehicleOptions");
var buttons = document.getElementsByTagName("button");
var fourWheel = document.getElementById("four_wheel_options");
var rentalCompany = /** @class */ (function () {
    function rentalCompany() {
        var _this = this;
        var _loop_1 = function (i) {
            buttons[i].addEventListener("click", function (e) { return _this.buttonEvent(Number(buttons[i].value)); });
        };
        for (var i = 0; i < buttons.length; i++) {
            _loop_1(i);
        }
    }
    rentalCompany.prototype.buttonEvent = function (n) {
        console.log("DEBUG - B/E" + n);
        showOptions.cleanSlate();
        switch (n) {
            case 20:
                fourWheel.style.display = "none";
                for (var _i = 0, _a = motorcycleRental.getAllofType(); _i < _a.length; _i++) {
                    var item = _a[_i];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 40:
                fourWheel.style.display = "block";
                break;
            case 60:
                fourWheel.style.display = "none";
                for (var _b = 0, _c = minibusRental.getAllofType(); _b < _c.length; _b++) {
                    var item = _c[_b];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 7:
                for (var _d = 0, _e = pickupRental.getAllofType(); _d < _e.length; _d++) {
                    var item = _e[_d];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 8:
                for (var _f = 0, _g = sedanRental.getAllofType(); _f < _g.length; _f++) {
                    var item = _g[_f];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 9:
                for (var _h = 0, _j = suvRental.getAllofType(); _h < _j.length; _h++) {
                    var item = _j[_h];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
            case 10:
                for (var _k = 0, _l = vanRental.getAllofType(); _k < _l.length; _k++) {
                    var item = _l[_k];
                    showOptions.addToStorage(item);
                    //console.log(item.brand+item.model);
                }
                break;
        }
        showOptions.addToDisplay();
        showOptions.display();
        var rentalOrder = new Order_1.Order();
        rentalOrder.hideReceipt();
    };
    return rentalCompany;
}());
var rentARide = new rentalCompany();

},{"./Minibus":1,"./Motorcycle":2,"./ObjectDisplay":3,"./Order":4,"./Pickup":5,"./SUV":6,"./Sedan":7,"./Van":8,"./VehicleList":9}]},{},[10]);
