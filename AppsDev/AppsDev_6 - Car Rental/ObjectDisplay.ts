import { Vehicle } from './Vehicle';

export class ObjectDisplay {
    protected target: any;
    protected storage: Array<Vehicle>;
    protected parsedData: string;
    protected currentID: number;

    constructor(input: string){
        this.target = document.getElementById(input); 
        this.storage = [];
        this.parsedData = "";
        this.currentID = 0;
    }

    addToStorage(input: Vehicle): void{
        this.storage.push(input);
    }

    addToDisplay(){
        let update : string = "" 
        for(let index of this.storage){
            update += "<div class=\"squareish\" id=\"option_"+this.currentID+"\"><h4>"+index.brand+" "+ index.model + "</h4><p>Php." +index.pricePerDay+ "/day</p>" + "<button id=\"RentOption_"+this.currentID+"\" value=\""+this.currentID+"\">RENT</button></div>";
            console.log(update);
            this.currentID +=1;
            console.log("currentID " + this.currentID);
        }
        this.parsedData = update;
    }

    cleanSlate(){
        this.storage = [];
        this.parsedData = "";
        this.currentID = 0;
        this.display();
    }

    display() {
        this.target.innerHTML = this.parsedData;
    }

}