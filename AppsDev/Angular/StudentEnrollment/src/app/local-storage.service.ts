import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  public tokenKey = 'studentDB';

  constructor() {}
  store(content: any) {
    // console.log('inside localstorage store:', content);
    localStorage.setItem(this.tokenKey, content);
  }

  retrieve() {
    // console.log('inside localstorage');
    let storedToken: any = localStorage.getItem(this.tokenKey);
    // console.log('storedToken:', storedToken);
    if (!storedToken) {
      const initData: Array<any> = [''];
      this.store(JSON.stringify(initData));
      console.log('Initializing blank storage');
      console.log(JSON.parse(this.retrieve()));
      storedToken = localStorage.getItem(this.tokenKey);
    }
    return storedToken;
  }
}
