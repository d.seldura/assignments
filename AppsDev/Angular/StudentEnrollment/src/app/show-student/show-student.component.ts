import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from './../local-storage.service';

@Component({
  selector: 'app-show-student',
  templateUrl: './show-student.component.html',
  styleUrls: ['./show-student.component.css']
})
export class ShowStudentComponent implements OnInit {
  private currentData: Array<any>;
  constructor(private localStorage: LocalStorageService) {}

  ngOnInit() {
    this.currentData = JSON.parse(this.localStorage.retrieve());
    console.log(this.currentData);
  }
}
