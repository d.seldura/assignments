import { Vehicle } from './Vehicle';

export class Minibus {
      protected minibusCollection: Array<Vehicle> = [];

      constructor(input: Array<Vehicle>){
             this.minibusCollection = input; 
      }

     getAllofType(): Array<Vehicle>{
           let minibusList: Array<Vehicle> = [];
           for(let index of this.minibusCollection){
               if(index.type=="minibus"){
                   minibusList.push(index);
               }
           }
           return minibusList;
     } 
}