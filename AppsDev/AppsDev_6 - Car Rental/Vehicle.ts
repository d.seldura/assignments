export interface Vehicle {
    noOfWheels: number;
    model: string;
    brand: string;
    type: string;
    pricePerDay: number;
}