/*1_1.*/
use sakila;
select staff.first_name as 'First Name', staff.last_name as 'Last Name', address.address as 'Address'
from staff
inner join address
ON address.address_id = staff.address_id;

/*1_2.*/
use sakila;
select concat(staff.first_name,' ',staff.last_name) as 'Staff', sum(payment.amount) as 'Total Amount'
from payment
inner join staff
on payment.staff_id = staff.staff_id
where payment_date like '2005-08%'
group by payment.staff_id;

/*1_3.*/
use sakila;
select film.title as 'Movie', count(film_actor.actor_id) as 'Actor Count'
from film
join film_actor
on film.film_id = film_actor.film_id
group by film.title;

/*1_4.*/
use sakila;
select film.title as Film, count(inventory.inventory_id) as 'Count of Film'
from film
join inventory
on film.film_id = inventory.film_id
where film.title = 'Hunchback Impossible'
group by film.film_id;


/*1_5.*/
use sakila;
select concat(customer.last_name,',',customer.first_name) as 'Customer', sum(payment.amount) as 'Total Payments Made'
from payment
join customer
on payment.customer_id = customer.customer_id
group by payment.customer_id
order by `Customer` asc;


/*Subqueries*/

/*2_1.*/
use sakila;
select film.title as `Movie Title`
from film
where film.language_id = (select language_id from language where name = 'English')
and film.title like 'K%' or 'Q%' ;

/*2_2.*/
use sakila;
select CONCAT(first_name,' ',last_name) as 'Actors who performed in "Alone Trip"'
from actor
where actor_id in 
(select actor_id from film_actor where film_id = 
(select film_id from film where title = 'Alone Trip'));

/*2_3.*/
use sakila;
select concat(customer.first_name,' ',customer.last_name) as 'Customer Name', customer.email as 'email'
from customer
inner join address on customer.address_id = address.address_id
inner join city on address.city_id = city.city_id
inner join country on country.country_id = city.country_id
where country.country = 'Canada';

/*2_4.*/
use sakila;
select film.title as 'Family Friendly Movies'
from film
join film_category on film_category.film_id = film.film_id
join category on category.category_id = film_category.category_id
where category.name = 'Family';

/*2_5.*/
use sakila;
SELECT title as `Movie Title`, COUNT(film.film_id) AS 'Rental count'
FROM  film	
JOIN inventory ON (film.film_id= inventory.film_id)
JOIN rental ON (inventory.inventory_id=rental.inventory_id)
GROUP BY title ORDER BY `Rental count` DESC;